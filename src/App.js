import {BrowserRouter, Route, Routes} from "react-router-dom";
import {lazy} from "react";
import {ROOT_PATH} from "./routes/Slugs";

const DefaultLayout = lazy(() => import('./components/layout/DefaultLayout'));

function App() {

  return (
      <BrowserRouter>
          <Routes>
              {/*<Route path={ROOT_PATH} element={<DefaultLayout/>}/>*/}
              <Route path='*' element={<DefaultLayout/>}/>
          </Routes>
      </BrowserRouter>
  );
}

export default App;
