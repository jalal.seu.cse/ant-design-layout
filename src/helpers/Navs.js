import {DesktopOutlined, FileOutlined, PieChartOutlined, TeamOutlined, UserOutlined} from "@ant-design/icons";
import React from "react";
import {DASHBOARD_PATH} from "../routes/Slugs";
// import * as path from "path";

function getItem(label, key, icon, children) {
    return {
        key,
        icon,
        children,
        label,
        // path
    };
}

const Navs = [
    // getItem(
    //     'Option 1',
    //     '1', <PieChartOutlined
    //     />
    // ),
    {
        key: 'dashboard',
        icon:  <PieChartOutlined />,
        label: 'dashboard',
        children: null,
        path: DASHBOARD_PATH
    },
    getItem('Option 2', '2', <DesktopOutlined />),
    getItem('User', 'sub1', <UserOutlined />, [
        getItem('Tom', '3'),
        getItem('Bill', '4'),
        getItem('Alex', '5'),
    ]),
    getItem('Team', 'sub2', <TeamOutlined />, [getItem('Team 1', '6'), getItem('Team 2', '8')]),
    getItem('Files', '9', <FileOutlined />),
]

export default Navs;