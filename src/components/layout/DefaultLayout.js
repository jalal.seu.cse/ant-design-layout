import React, {useState} from 'react';
import {Layout, Menu} from 'antd';
import Navs from '../../helpers/Navs';
import {Route, Routes} from "react-router-dom";
import AppRoutes from "../../routes/AppRoutes";

const { Header, Content, Footer, Sider } = Layout;

const DefaultLayout = () => {
    const [collapsed, setCollapsed] = useState(false);
    return (
        <Layout
            style={{
                minHeight: '100vh',
            }}
        >
            <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
                <div className="logo" />
                <Menu
                    theme="dark
                    " defaultSelectedKeys={['1']}
                    mode="inline" items={Navs}
                />
            </Sider>
            <Layout className="site-layout">
                <Header
                    className="site-layout-background"
                    style={{
                        padding: 0,
                    }}
                />
                <Content
                    style={{
                        margin: '0 16px',
                    }}
                >

                    <Routes>
                        {
                            AppRoutes.map(route => {

                                // if (!hasPermission(permissions, route.permissions)) {
                                //     return null;
                                // }

                                return <Route
                                    key={route.path}
                                    path={route.path}
                                    element={<route.component/>}
                                    // element={route.components}
                                />
                            })
                        }
                    </Routes>


                    {/*<Breadcrumb*/}
                    {/*    style={{*/}
                    {/*        margin: '16px 0',*/}
                    {/*    }}*/}
                    {/*>*/}
                    {/*    <Breadcrumb.Item>User</Breadcrumb.Item>*/}
                    {/*    <Breadcrumb.Item>Bill</Breadcrumb.Item>*/}
                    {/*</Breadcrumb>*/}
                    {/*<div*/}
                    {/*    className="site-layout-background"*/}
                    {/*    style={{*/}
                    {/*        padding: 24,*/}
                    {/*        minHeight: 360,*/}
                    {/*    }}*/}
                    {/*>*/}
                    {/*    Bill is a cat.*/}
                    {/*</div>*/}
                </Content>
                <Footer
                    style={{
                        textAlign: 'center',
                    }}
                >
                    Ant Design ©2018 Created by Ant UED
                </Footer>
            </Layout>
        </Layout>
    );
};

export default DefaultLayout;