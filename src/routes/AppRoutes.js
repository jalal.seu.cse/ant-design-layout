import {DASHBOARD_PATH} from "./Slugs";
import {lazy} from "react";
import Dashboard from "../components/pages/Dashboard/Dashboard";

// const Dashboard = lazy(()  => import('../components/pages/Dashboard/Dashboard'))

const AppRoutes = [
    {
        path: DASHBOARD_PATH,
        // components: Dashboard
        component: Dashboard
    }
]

export default AppRoutes;